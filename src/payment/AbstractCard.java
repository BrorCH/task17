package payment;

import payment.iMoney;


public abstract class AbstractCard implements iMoney {
    private String CardNumber;
    private String pin;
    private String expiryDate;
    private String cvc;
    private String cardHolder;
    // in Noroff Krowns
    double saldo = 0;

    public AbstractCard() {
        this.CardNumber = "3201-4567-0998-4560";
        this.pin = "4568";
        this.expiryDate = "12/20";
        this.cvc = "345";
        this.cardHolder = "Craig Marais";
        this.saldo = 10.0;
    }

    public AbstractCard(String creditCard) {
        this.CardNumber = "3201-4567-0998-4560";
        this.pin = "4568";
        this.expiryDate = "12/20";
        this.cvc = "345";
        this.cardHolder = "Craig Marais";
    }

    public AbstractCard(String cardNumber, String pin, String expiryDate, String cvc, String cardHolder, double saldo) {
        this.CardNumber = cardNumber;
        this.pin = pin;
        this.expiryDate = expiryDate;
        this.cvc = cvc;
        this.cardHolder = cardHolder;
        this.saldo = saldo;
    }

    public AbstractCard(String cardNumber, String pin, String expiryDate, String cvc, String cardHolder) {
        this.CardNumber = cardNumber;
        this.pin = pin;
        this.expiryDate = expiryDate;
        this.cvc = cvc;
        this.cardHolder = cardHolder;

    }

    private double transaction(double price) {
        if (this.getSaldo() > price) {
            return setSaldo(this.getSaldo() - price);
        } else {
            System.out.println("Payment denied! You only have: " + this.getSaldo()
                    + " Noroff Krowns in your account");
            return this.getSaldo();
        }
    }

    public double getSaldo() {
        return saldo;
    }

    public double setSaldo(double saldo) {
        this.saldo = saldo;
        return this.saldo;
    }

    public String getUser() {
        return this.cardHolder;
    }

    public void abortTransaction() {
        System.out.println("Calling the police... please hold");
        System.exit(0);
    }

    public double pay(AbstractCard c, double price){
        if(verifyCard(c)){
            return transaction(price);
        }else{
            abortTransaction();
            return this.getSaldo();
           
        }
    }

    private boolean verifyCard(AbstractCard card){
        if(!(this.CardNumber == card.getCardNumber())){
            return false;
        }
        if(!(this.pin == card.getPin())){
            return false;
        }
        if(!(this.expiryDate == card.getExpiryDate())){
            return false;
        }
        if(!(this.cvc == card.getCvc())){
            return false;
        }
        if(!(this.cardHolder == card.getCardHolder())){
            return false;
        }
        return true;
    }

    public String getCardHolder() {
        return this.cardHolder;
    }

    public String getCvc() {
        return this.cvc;
    }

    public String getExpiryDate() {
        return this.expiryDate;
    }

    public String getPin() {
        return this.pin;
    }

    public String getCardNumber() {
        return this.CardNumber;
    }
}