package payment;

import payment.AbstractCard;

public class SavingsCard extends AbstractCard {

    public SavingsCard() {
        super();
    }

    public SavingsCard(String cardNumber, String pin, String expiryDate, String cvc, String cardHolder, double saldo) {
        super(cardNumber, pin, expiryDate, cvc, cardHolder, saldo);
    }
}