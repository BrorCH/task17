package payment;

import payment.iMoney;

public class Cash implements iMoney{
    
    // in Noroff Krowns
    double saldo = 0;

    public Cash(double saldo){
        this.saldo = saldo;
    }

    public double pay(int price){
        if(this.saldo>price){
            return this.setSaldo(this.getSaldo()-price);
        }else{
            abortTransaction();
            return this.saldo;
        }
    }

    @Override
    public double getSaldo() {  
        return this.saldo;
    }

    @Override
    public double setSaldo(double saldo) {
       this.saldo = saldo;
        return this.saldo;
    }

    public String getChange(){
        double tempSaldo = this.saldo;
        return recursiveChange(tempSaldo);

    }

    private String recursiveChange(double saldo){
        if(saldo>1000){
            System.out.println(saldo);
            return (int)(saldo/1000)+ ", 1000 Noroff Krown bill(s)\n" + recursiveChange(saldo%1000);
        }else if(saldo>500){
            System.out.println(saldo);
            return "1, 500 Noroff krown bill\n" + recursiveChange(saldo%500);
        }else if(saldo>200){
            System.out.println(saldo);
            return (int)(saldo/200)+ ", 200 Noroff krown bill(s)\n" + recursiveChange(saldo%200);
        }else if(saldo>100){
             System.out.println(saldo);
            return "1, 100 Noroff Krown bill\n" + recursiveChange(saldo%100);
        }else if(saldo>50){
             System.out.println(saldo);
            return "1, 50 Noroff Krown bill\n" + recursiveChange(saldo%50);
        }else if(saldo>20){
             System.out.println(saldo);
            return (int)(saldo/20)+ ", 20 Noroff krown coin(s)\n" + recursiveChange(saldo%20);
        }else if(saldo>10){
             System.out.println(saldo);
            return "1, 10 Noroff Krown coin\n" + recursiveChange(saldo%10);
        }else if(saldo>5){
             System.out.println(saldo);
            return "1, 5 Noroff Krown coin\n" + recursiveChange(saldo%5);
        }else{
             System.out.println(saldo);
            return (int)saldo + ", 1 Noroff Krown coin(s)";
        }
    }

    @Override
    public void abortTransaction() {
        System.out.println("Not enough cash in hand.");
    }

}