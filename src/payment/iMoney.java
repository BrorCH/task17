package payment;

public interface iMoney{

    private double transaction(){
        return 0;
    }

    double getSaldo();

    double setSaldo(double saldo);

    void abortTransaction();
}