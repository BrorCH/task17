package payment;

import payment.AbstractCard;

public class CreditCard extends AbstractCard{

    public CreditCard(){
        super("creditCard");
        this.saldo = 15000;
    }

    public CreditCard(String cardNumber, String pin, String expiryDate, String cvc, String cardHolder){
        super(cardNumber, pin, expiryDate, cvc, cardHolder);
    }
}
