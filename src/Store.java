import java.util.ArrayList;
import java.util.Scanner;
import payment.*;

public class Store {
    private static ArrayList<Items> inStore;
    private static ArrayList<Items> cart;
    private static boolean isShopping;
    private static CreditCard card;
    private static ArrayList<AbstractCard> cardsDb;
    private static boolean freeCardMade = false;
    private static Cash cash;

    public static void main(String args[]) {
        card = new CreditCard();
        cardsDb = new ArrayList<>();
        cardsDb.add(card);

        // System.out.println(card.getCardNumber() + " " + card.getPin() + " " +
        // card.getExpiryDate() + " " + card.getCvc() +" "+ card.getCardHolder() + " "+
        // card.getSaldo());
        inStore = new ArrayList<>();
        cart = new ArrayList<>();
        isShopping = true;

        inStore.add(new Items("Apple", 10));
        inStore.add(new Items("Iphone", 14));
        inStore.add(new Items("Fanta", 20));
        inStore.add(new Items("Pepsi", 100));
        inStore.add(new Items("Orangina", 98));
        inStore.add(new Items("Craig memes", 1));

        System.out.println("Your auto generated credit card is:");
        System.out.println("Card number: " + card.getCardNumber());
        System.out.println("Card pin: " + card.getPin());
        System.out.println("Card expiry date: " + card.getExpiryDate());
        System.out.println("Card CVC: " + card.getCvc());
        System.out.println("Card holders name: " + card.getCardHolder());
        System.out.println("Card saldo: " + card.getSaldo() + " Noroff Krowns");

        Scanner in = new Scanner(System.in);
        while (isShopping)
            try {
                greetings(in);
            } catch (Exception e) {
                greetings(in);
            }

    }

    public static void greetings(Scanner in) {
        System.out.println("Welcome to SuperStore!!");
        System.out.println("Today Only 50% off on all Craig memes");
        System.out.println("Here you can buy:");
        System.out.println("Apple 10 Noroff Krowns");
        System.out.println("Iphone 14 Noroff Krowns");
        System.out.println("Fanta 20 Noroff Krowns");
        System.out.println("Pepsi 100 Noroff Krowns");
        System.out.println("Orangina 98 Noroff Krowns");
        System.out.println("Craig Memes 1 Noroff Krown");
        System.out.println("Type in what you want one item at a time, or type 'Cart' to see your cart");

        String item = in.nextLine();
        boolean addedSomethingToCart = false;
        for (int i = 0; i < inStore.size(); i++) {
            if (item.toLowerCase().contains(inStore.get(i).getItem().toLowerCase())) {
                System.out.println("\n" + item + " added to cart\n");
                cart.add(inStore.get(i));
                addedSomethingToCart = true;
            }
        }

        if (item.contains("Cart") || item.substring(0, 1).toLowerCase().equals("c") && !addedSomethingToCart) {
            showCart(in);
        }

    }

    public static void showCart(Scanner in) {

        if (cart.isEmpty()) {
            System.out.println("Your cart is empty");
        } else {
            System.out.println("In your cart are the following items: ");
        }
        int price = 0;
        for (int i = 0; i < cart.size(); i++) {
            System.out.println(cart.get(i).getItem());
            price += cart.get(i).getPrice();
        }

        System.out.println("Total price: " + price);

        System.out.println("Do you want to keep browsing or go to checkout?");
        System.out.println("Type 'Browsing' or 'checkout'");

        String response = in.nextLine();
        if (response.toLowerCase().contains("browsing") || response.substring(0, 1).toLowerCase().equals("b")) {
            greetings(in);
        } else {
            checkout(price, in);
        }

    }

    public static void finished(Scanner in) {
        System.out.println("Do you want to continue shopping? y/n");
        if (in.nextLine().toLowerCase().equals("y")) {
            greetings(in);
        } else {
            System.out.println("Thank you for using SuperStore, your goods will be delivered to someone else.");
            isShopping = false;
        }
    }

    public static void checkout(double price, Scanner in) {
        System.out.println("How do you wish to pay?");
        System.out.println("For Credit card press:          'C'");
        System.out.println("For savings card press:         'S'");
        System.out.println("If you have cash in hand press: 'H'");
        System.out.println("\nContinue shopping press:        'B'");

        String payment = in.nextLine();

        if (payment.toLowerCase().equals("c")) {
            payWithCreditCard(price, in);
        } else if (payment.toLowerCase().equals("s")) {
            payWithSavingsCard(price, in);
        } else if (payment.toLowerCase().equals("h")) {
            payWithCash(price, in);
        } else if (payment.toLowerCase().equals("b")) {
            greetings(in);
        } else {
            checkout(price, in);
        }

    }

    private static void payWithSavingsCard(double price, Scanner in) {
        boolean foundACard = false;

        for (AbstractCard card : cardsDb) {
            if (card instanceof SavingsCard) {
                System.out.println("Is this the card you want to pay with? y/n");
                System.out.println(card.getCardNumber());
                foundACard = true;
                if (in.nextLine().toLowerCase().equals("y")) {
                    double money = card.getSaldo();
                    System.out.println("You have " + card.pay(card, price) + " Noroff Krowns left on your account");
                    if (money > card.getSaldo()) {
                        cart.clear();
                    }
                    finished(in);
                } else {
                    foundACard = false;
                }
            }
        }
        if (!foundACard) {
            System.out.println("Do you want to register a new card. y/n");
            if (in.nextLine().toLowerCase().equals("y")) {
                registerCard(in);
            } else {
                if (!freeCardMade) {
                    System.out.println(freeCardMade);
                    SavingsCard newCard = new SavingsCard();
                    cardsDb.add(newCard);
                    System.out.println("We made you this card " + newCard.getCardNumber() + "\nIt has "
                            + newCard.getSaldo() + " Noroff Krowns");
                    freeCardMade = true;
                }
                checkout(price, in);
            }
        }

    }

    private static void registerCard(Scanner in) {
        System.out.println("please enter your card number:");
        String cardNumber = in.nextLine();
        System.out.println("please enter your pin:");
        String pin = in.nextLine();
        System.out.println("please enter the expiry date:");
        String expDat = in.nextLine();
        System.out.println("please enter the cvc number:");
        String cvc = in.nextLine();
        System.out.println("please enter your name:");
        String name = in.nextLine();
        System.out.println("please enter amount of money you would like on the card:");
        String money = in.nextLine();

        SavingsCard newCard = new SavingsCard(cardNumber, pin, expDat, cvc, name, Double.parseDouble(money));
        cardsDb.add(newCard);
    }

    private static void payWithCreditCard(double price, Scanner in) {
        double orderFinished = card.getSaldo();
        System.out.println("You have " + card.pay(card, price) + "left on your credit card");
        if (card.getSaldo() < orderFinished) {
            cart.clear();
        }
        finished(in);

    }

    private static void payWithCash(double price, Scanner in) {
        System.out.println("You have to pay: " + price + " Noroff Krowns");
        System.out.println("How many Noroff Krowns are you giving me?");
        String noroffKrowns = in.nextLine();
        double moneyInMyPocket = 0;
        try {
            moneyInMyPocket = Double.parseDouble(noroffKrowns);
        } catch (NumberFormatException ne) {
            System.out.println("you did not pass in a valid number.");
            checkout(price, in);
        } catch (NullPointerException ne) {
            System.out.println("you did not pass in anything.");
            checkout(price, in);
        }

        if (price < moneyInMyPocket) {
            cash = new Cash(moneyInMyPocket - price);
            System.out.println("You get back:\n" + cash.getChange());
            cart.clear();
        } else {
            System.out.println("You should use the credit card");
        }

        finished(in);

    }
}
